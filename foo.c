#include <stdio.h>

#ifndef LAMBDA
#define LAMBDA 1
#endif

int main(){
	int index = LAMBDA;
	int array[10];
	array[index] = 0;
	printf("array[%d] is %d\n", index ,array[index]);
	return 0;
}